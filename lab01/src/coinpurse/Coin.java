package coinpurse;
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Piatpol.T
 */
public class Coin implements Comparable<Valuable>,Valuable{

    /** Value of the coin. */
    private double value;
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ) {
        this.value = value;
    }


    /**
     * @param obj is the thing that you want to equal.
     * Is true if a) Object is not null, b) Object is a Coin, c) Object has same value as this Coin.
     * @return true when value of coin is the same  
     */
    public boolean equals(Object obj)
    {
    	if(obj == null)
    	{
    		return false;
    	}
    	if(obj.getClass() != this.getClass())
    	{
    		return false;
    	}
    	Coin other = (Coin) obj;
    	if(value == other.value)
    	{
    		return true;
    	}
    	return false;
    }
    /**
     * Compare Coins.
     * @param obj is the thing that you want to compare.
     * @return 1) -1 when value less than 2) 0 when same value 3) 1 when value greater than other 4) when null return Coin must not be null
     */
    public int compareTo(Valuable obj)
    {
    	Coin other = (Coin)obj;
    	if(this.getValue() < other.getValue())
    	{
    		return -1;
    	}
    	else if(this.getValue() > other.getValue())
    	{
    		return 1;
    	}
    	else if(this.getValue() == other.getValue())
    	{
    		return 0;
    	}
    	else if(obj == null)
    	{
    		throw new NullPointerException("Coin must not be null");
    	}
    	return -1;
    }

    /**
     * @return true if obj is a Coin and has the same value as this coin.
     */
    public double getValue( )
    {
    	return this.value;
    }
    /**
     * @return Show the coin value in Baht.
     */
    public String toString()
    {
    	return (int)this.value+"-Baht Coin.";
    }   
}