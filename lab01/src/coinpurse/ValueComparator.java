package coinpurse;

import java.util.Comparator;

/**
 * The comparator that compare between Valuable object.
 * @author Pipatpol Tanavngchinda
 *
 */
public class ValueComparator implements Comparator<Valuable>{
	/**
	 * Compare 2 string a and b.
	 * @return < 0 if a is before b, = 0 if a and b are same,
	 * 		> 0 if a is after b.
	 */
	public int compare(Valuable a, Valuable b)
	{
		return (int) Math.signum( a.getValue() - b.getValue() );
	}
}
